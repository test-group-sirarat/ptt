import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { MainPageComponent } from './home/main-page/main-page.component'
import { UserMComponent } from './home/user-m/user-m.component'
import { DashBoardComponent } from './home/dash-board/dash-board.component'
const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'gis',
    loadChildren: () => import('./gis/gis.module').then((m) => m.GisModule),
    data: {
      systemId: 'GIS',
    },
  },
  {path: 'mainPage' , component:   MainPageComponent},
  {path: 'dashBoard' , component:   DashBoardComponent},
  {path: 'userM' , component:  UserMComponent},


  
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
