import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { AppRoutingModule } from './app-routing.module'
import { AxAuthenticationModule } from '@atlasx/core/authentication'
import { AxConfigurationModule } from '@atlasx/core/configuration'
import { AxWebServiceUrl } from '@atlasx/core/http-service'
import { AppComponent } from './app.component'
import { environment } from '../environments/environment'
import { ArcgisjsapiProvider } from './gis/argisjsapi-provider';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DashBoardComponent } from './home/dash-board/dash-board.component';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {CardModule} from 'primeng/card';
import {MenubarModule} from 'primeng/menubar';
import {ButtonModule} from 'primeng/button';
import { MainPageComponent } from './home/main-page/main-page.component';
import {TabViewModule} from 'primeng/tabview';
import {InputTextModule} from 'primeng/inputtext';
import {PasswordModule} from 'primeng/password';
import { UserMComponent } from './home/user-m/user-m.component';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import {CheckboxModule} from 'primeng/checkbox';

import {RadioButtonModule} from 'primeng/radiobutton';
import {ListboxModule} from 'primeng/listbox';
import { EditUserComponent } from './home/edit-user/edit-user.component';

import {ToastModule} from 'primeng/toast';
import {RippleModule} from 'primeng/ripple';


import { Ng2SearchPipeModule } from 'ng2-search-filter';









@NgModule({
  declarations: [AppComponent,DashBoardComponent, MainPageComponent, UserMComponent, EditUserComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ButtonModule,

    // Required register, if application use AtlasX configuration pattern.
    // It will load configuration before application initial startup.
    AxConfigurationModule,

    // Required register, if application use authentication.
    AxAuthenticationModule.forRoot(environment),
    ScrollPanelModule,
    CardModule,
    MenubarModule,
    TabViewModule,
    InputTextModule,
    PasswordModule,
    TableModule,
    DropdownModule,
    FormsModule,
    RadioButtonModule,
    CheckboxModule,
    ListboxModule,
    ToastModule,
    RippleModule,
    Ng2SearchPipeModule

  
  ],
  providers: [
    // Required register, if application use AxAuthenticationModule or AxConfigurationModule.
    { provide: AxWebServiceUrl, useValue: environment.webServiceUrl },
    

    // Requried register, if application use ArcGIS API for JavaScript.
    ArcgisjsapiProvider,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
