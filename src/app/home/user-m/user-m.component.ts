import { createViewChild } from '@angular/compiler/src/core';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AxRequestService } from '@atlasx/core/http-service'
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
@Component({
  selector: 'app-user-m',
  templateUrl: './user-m.component.html',
  styleUrls: ['./user-m.component.scss'],
  providers: [MessageService]
})
export class UserMComponent implements OnInit {

  @ViewChild("table") table: Table

  @Input() user_id: number
  @Input() user_name: string
  @Input() emp_code: string
  @Input() name: string
  @Input() surname: string
  @Input() company: string
  @Input() job_title: string
  @Input() email: string
  @Input() mobile: string

  searchText;





  prefixs: Prefix[];
  selectedPrefix: Prefix;

  selectedGroup: Prefix;
  groupSelect: Groups[];

  users: Data[];
  groups: DataGroup[];


  // datas: Datas[];

  datas: { USER_ID: string, USERNAME: string, EMP_CODE: string, NAME: string, SURNAME: string, MOBILE: string, JOB_TITLE: string, EMAIL: string, DATE_MODIFIED: string, DATE_CREATED: string, COMPANY: string }[] = []
  // group: {}



  ///


  constructor(private requesService: AxRequestService, private messageService: MessageService) {
    this.prefixs = [
      { name: 'นาง', code: '' },
      { name: 'นาย', code: '' },
      { name: 'นางสาว', code: '' }
    ]


    this.groupSelect = [
      { name: 'Assistant Manager', code: '' },
      { name: 'Manger', code: '' },
      { name: 'Senior', code: '' },
      { name: 'officer', code: '' },

    ]




  }




  ngOnInit(): void {

    this.testSP()
    // this.testSPGroup()

    if (this.table.totalRecords === 0) {
      this.table.currentPageReportTemplate = this.table.currentPageReportTemplate.replace("{first}", "0")
    }

    // this.users = [
    //   { username: 'xxxx123', code: '001001', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Assistant Manager', date: 'วว/ดด/ปป' },
    //   { username: 'xxxx124', code: '001002', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Senior', date: 'วว/ดด/ปป' },
    //   { username: 'xxxx125', code: '001003', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Supervisor', date: 'วว/ดด/ปป' },
    //   { username: 'xxxx126', code: '001004', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Assistant Manager', date: 'วว/ดด/ปป' },
    //   { username: 'xxxx127', code: '001005', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Senior', date: 'วว/ดด/ปป' },
    //   { username: 'xxxx128', code: '001006', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Senior', date: 'วว/ดด/ปป' },
    //   { username: 'xxxx129', code: '001007', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Officer', date: 'วว/ดด/ปป' },
    //   { username: 'xxxx130', code: '001008', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Supervisor', date: 'วว/ดด/ปป' },
    //   { username: 'xxxx131', code: '001009', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Assistant Manager', date: 'วว/ดด/ปป' },
    //   { username: 'xxxx132', code: '001010', name: 'xxxx xxxx', company: 'company', position: 'xxxxxxx', email: 'work@email.com', phone: '088-123-4567', group: 'Assistant Manager', date: 'วว/ดด/ปป' }
    // ],

    this.groups = [
      { groupName: 'ผู้บริหารระดับสูง', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
      { groupName: 'เจ้าหน้าที่จัดการเคส', detail: 'xxxxxxxxxxxxxxx', membersNumber: 8, date: 'วว/ดด/ปป' },
      { groupName: 'เจ้าหน้าที่มอนิเตอร์', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
      { groupName: 'ผู้บริหาร', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
      { groupName: 'เจ้าหน้าที่หน่วยงานภายนอก', detail: 'xxxxxxxxxxxxxxx', membersNumber: 5, date: 'วว/ดด/ปป' },
      { groupName: 'ผู้บริหารหน่วยงานภายนอก', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
      { groupName: 'เจ้าหน้าที่ติดตาม', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
      { groupName: 'เจ้าหน้าที่ตรวจสอบ', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
      { groupName: 'ผู้บริหาร PTT Digital', detail: 'xxxxxxxxxxxxxxx', membersNumber: 2, date: 'วว/ดด/ปป' },
    ]



  }


  testSP() {
    console.log("TestSP")
    let params = {
    }
    this.requesService.sp("PTT_UM_USER_Q", "POST", params).toPromise().then((response) => {
      console.log("requestService response = ", response)
      if (response["data"].length > 0) {
        for (let user of response["data"]) {
          this.datas.push({ USER_ID: user.USER_ID, USERNAME: user.USERNAME, EMP_CODE: user.EMP_CODE, NAME: user.NAME, SURNAME: user.SURNAME, MOBILE: user.MOBILE, JOB_TITLE: user.JOB_TITLE, EMAIL: user.EMAIL, DATE_MODIFIED: user.DATE_MODIFIED, DATE_CREATED: user.DATE_CREATED, COMPANY: user.COMPANY })
          // this.count++;

        }
      }
      console.log(this.datas)
    })
  }


  testSPGroup() {
    console.log("TestSPGroup")
    let params = {
    }
    this.requesService.sp("PTT_UM_ROLE_Q", "POST", params).toPromise().then((response) => {
      console.log("requestService response = ", response)


    })
  }





  div1: boolean = true;
  div2: boolean = true;
  check: number = 0
  div1Function() {
    this.check = 1
    this.div1 = true;
    this.div2 = false;
  }

  div2Function() {
    this.check = 2
    this.div2 = true;
    this.div1 = false;
  }

  //////////
  div3: boolean = true;
  div4: boolean = true;
  checkG: number = 0
  div3Function() {
    this.checkG = 1
    this.div3 = true;
    this.div4 = false;
  }

  div4Function() {
    this.checkG = 2
    this.div4 = true;
    this.div3 = false;
  }



  AddData(user_id: number, user_name: string, emp_code: string, name: String, surname: string, company: string, job_title: string, email: string, mobile: string) {
    let params = {
      USER_ID: user_id,
      USERNAME: user_name,
      EMP_CODE: emp_code,
      NAME: name,
      SURNAME: surname,
      COMPANY: company,
      JOB_TITLE: job_title,
      EMAIL: email,
      MOBILE: mobile

    }
    console.log("addUser response = ", params)

    this.requesService.sp("PTT_UM_USER_I", "GET", params).toPromise().then((response) => {
      console.log("addUser response = ", response)
      this.testSP()
    })
  }

  showSuccess() {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'เพิ่มสำเร็จ' });
  }

  clearData() {
    this.user_name = ''
    this.emp_code = ''
    this.name = ''
    this.surname = ''
    this.company = ''
    this.job_title = ''
    this.email = ''
    this.mobile = ''




  }


}
export interface Data {
  username?: string,
  code?: string,
  name?: string,
  company?: string,
  position?: string,
  email?: string,
  phone?: string
  group?: string
  date?: string

}

export interface DataGroup {
  groupName?: string,
  detail?: string,
  membersNumber?: number
  date?: string

}



interface Prefix {
  name: string,
  code: string
}


interface Groups {
  name: string,
  code: string
}

